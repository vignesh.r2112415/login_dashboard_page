const loginBtn = document.querySelector(".login-btn");
const userNameInput = document.querySelector(".userName-input");
const passwordInput = document.querySelector(".password-input");

const userNameErrorMessage = document.querySelector(".error-message-username");
const passwordErrorMessage = document.querySelector(".error-message-password");
const invalidErrorMessage = document.querySelector(".error-message-invalid");

const errorMessags = document.querySelectorAll(".error-messages");

const loginCreds = [{
    username: "vignesh",
    password: "xyz123",
},
{
    username: "vishal",
    password: "abc123",
},
{
    username: "admin",
    password: "admin",
}
];

let currentUser;

loginBtn && loginBtn.addEventListener("click", (e) => {
    e.preventDefault();
    validateInputField();
})

function validateInputField() {
    errorMessags.forEach((message) => {
        message.classList.remove("active");
    })

    if (userNameInput.value === "" && passwordInput.value === "") {
        userNameErrorMessage.classList.add("active");
        passwordErrorMessage.classList.add("active");
        return false;
    } else if (userNameInput.value === "") {
        userNameErrorMessage.classList.add("active");
        return false
    } else if (passwordInput.value === "") {
        passwordErrorMessage.classList.add("active");
        return false;
    }

    validateUserCredentials()
}


function validateUserCredentials() {
    loginCreds.forEach((user) => {
        const url = window.location.href.split("/");
        url.splice(-1);
        url.push("dashboard.html");
        const newUrl = url.join("/");

        if (user.username === userNameInput.value.trim() && user.password === passwordInput.value.trim()) {
            currentUser = user;
            window.localStorage.setItem("currentUser", btoa(JSON.stringify(currentUser)));
            window.open(newUrl, "_self");
        } else {
            invalidErrorMessage.classList.add("active");
        }
    });
}
